package stopwatch;

/**
 * Append to String test for speed test.
 * @author Chinthiti Wisetsombat
 * @version 1.1
 */
public class AppendToString implements Runnable{
	
	
	private static final char CHAR = 'a';
	/** name of the test.*/
	private static final String NAME = "Append to String";
	/** number of times that String is appended.*/
	private int counter;
	
	/**
	 * Constructor for this test.
	 * @param counter number of times that String will be appended
	 */
	public AppendToString(int counter){
		this.counter = counter;
	}
	
	@Override
	/**
	 * run the test
	 */
	public void run(){
		String s = "";
		for(int i = 0; i < counter ; i++){
			s += CHAR;
		}
	}
	
	/**
	 * give the information of the test.
	 * @return test's information
	 */
	public String toString(){
		return String.format("%s with count=%d", NAME , counter);
	}
}
