package stopwatch;

/**
 * Double(primitive) summation test for speed test.
 * @author Chinthiti Wisetsombat
 * @version 1.1
 */
public class SumDoublePrimitive implements Runnable{

	/** name of the test. */
	private static final String NAME = "Sum double primitive";
	/** double array that will be used to get the summation. */
	private double[] values ;
	/** number of times of the summation. */
	private int counter ;
	
	/**
	 * constructor for double summation test.
	 * @param counter number of times of the summation
	 * @param arraySize double array's size that will be used in the test
	 */
	public SumDoublePrimitive (int counter, int arraySize){
		this.counter = counter;
		this.values = new double[arraySize];
		for(int i = 0 ; i < arraySize ; i++){
			values[i] = i+1;
		}
	}
	
	/**
	 * run the test.
	 */
	public void run(){
		double sum = 0;
		for(int i = 0, count = 0; count < values.length; i++,count++){
			if(i >= values.length) i = 0;
			sum += values[i];
		}
	}
	
	/**
	 * give the test's information.
	 * @return test's information
	 */
	public String toString(){
		return String.format("%s with count=%d and array size=%d", NAME, counter , values.length);
	}
}
