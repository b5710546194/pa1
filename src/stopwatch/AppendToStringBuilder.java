package stopwatch;

/**
 * Append to StringBuilder test for speed test.
 * @author Chinthiti Wisetsombat
 * @version 1.1
 */
public class AppendToStringBuilder implements Runnable{

	private static final char CHAR = 'a';
	/** name of this test.*/
	private static final String NAME = "Append to StringBuilder";
	/** number of times that StringBuilder will be appended. */
	private int counter;
	
	/**
	 * Constructor for this test.
	 * @param counter number of time that StringBuilder will be appended
	 */
	public AppendToStringBuilder(int counter){
		this.counter = counter;
	}
	
	/**
	 * run this test.
	 */
	public void run(){
		StringBuilder builder = new StringBuilder();
		for(int i = 0; i < counter ; i++){
			builder = builder.append(CHAR);
		}
	}
	
	/**
	 * give information of this test.
	 * @return information of this test
	 */
	public String toString(){
		return String.format("%s with count=%d", NAME, counter);
	}
}
