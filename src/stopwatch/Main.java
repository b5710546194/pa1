package stopwatch;

import stopwatch.TaskTimer;

/**
 * Application class for testing TaskTimer and Tasks.
 * @author Chinthiti Wisetsombat
 * @version 1.1
 */
public class Main {

	/** initialize tasks , measure and print.*/
	public static void main (String[]args){
		// initialize tasks
		AppendToString task1 = new AppendToString(100000);
		AppendToStringBuilder task2 = new AppendToStringBuilder(100000);
		SumDoublePrimitive task3 = new SumDoublePrimitive(100000000,500000);
		SumDouble task4 = new SumDouble(100000000,500000);
		SumBigDecimal task5 = new SumBigDecimal(100000000,500000);
		
		//get a instance of TaskTimer
		TaskTimer timer = TaskTimer.getTaskTimer();
		
		//run the tasks and print the results
		timer.measureAndPrint(task1);
		timer.measureAndPrint(task2);
		timer.measureAndPrint(task3);
		timer.measureAndPrint(task4);
		timer.measureAndPrint(task5);
	}
}
