package stopwatch;

/**
 * A stopwatch that measures elapsed time between a starting time
 * and stopping time, or until the present time.
 * @author Chinthiti Wisetsombat
 * @version 1.1
 */
public class StopWatch {
	/** constant for converting nanoseconds to seconds. */
	private static final double NANOSECONDS = 1.0E-9;
	/** time that the stopwatch has started, in nanoseconds.*/
	private long startTime;
	/** time that the stopwatch has stopped, in nanoseconds.*/
	private long stopTime;
	/** stopwatch's status. */
	private boolean running;
	
	/**
	 * Constructor for a new stopwatch.
	 */
	public StopWatch(){
		this.running = false;
	}
	
	/**
	 * give a constant for converting nanoseconds to seconds.
	 * @return a nanosecond in unit "second"
	 */
	public double getNanoseconds(){
		return this.NANOSECONDS;
	}
	
	/**
	 * give start time, in nanoseconds.
	 * @return start time in nanoseconds.
	 */
	public long getStartTime(){
		return this.startTime;
	}
	
	/**
	 * give stop time, in nanoseconds.
	 * @return stop time in nanoseconds.
	 */
	public long getStopTime(){
		return this.stopTime;
	}
	
	/**
	 * checking that stopwatch is running or not.
	 * @return true if it is running, otherwise false
	 */
	public boolean isRunning(){
		return this.running;
	}
	
	/**
	 * reset and start the stopwatch. (if it isn't running)
	 */
	public void start(){
		if(!running){
			this.startTime = System.nanoTime();
			this.running = true;
		}
	}
	
	/**
	 * stop the stopwatch. (if it is running)
	 */
	public void stop(){
		if(running){
			this.stopTime = System.nanoTime();
			this.running = false;
		}
	}
	
	/**
	 * get the elasped time.
	 * @return time difference between start time and present time if stopwatch is running ,or between start time and stop time if stopwatch has stopped
	 */
	public double getElapsed(){
		if(running){
			long now = System.nanoTime();
			return (now-getStartTime())*NANOSECONDS;
		}
		return (getStopTime()-getStartTime())*NANOSECONDS;
	}
}
