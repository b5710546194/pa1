package stopwatch;

/**
 * A task timer that run task and measure the time that the task takes.
 * @author Chinthiti Wisetsombat
 * @version 1.1
 */
public class TaskTimer {
	
	/** StopWatch that is used to measure the time. */
	private static final StopWatch TIMER = new StopWatch();
	
	/**
	 * private constructor.
	 */
	private TaskTimer(){
		
	}
	
	/**
	 * run the task that implements Runnable, measure the time and print the result.
	 * @param runnable the task that implements Runnable
	 */
	public static void measureAndPrint(Runnable runnable){
		TIMER.start();
		runnable.run();
		TIMER.stop();
		
		System.out.println("Task : " + runnable.toString());
		System.out.printf("Elapsed time %.6f sec \n\n",TIMER.getElapsed());
	}
	
	/**
	 * Return a instance of TaskTimer.
	 * @return instance of TaskTimer
	 */
	public static TaskTimer getTaskTimer(){
		return new TaskTimer();
	}
}
