package stopwatch;

/**
 * Double summation test for speed test.
 * @author Chinthiti Wisetsombat
 * @version 1.1
 */
public class SumDouble implements Runnable{
	
	/** name of the test. */
	private static final String NAME = "Sum Double";
	/** Double array that will be used to get the summation. */
	private Double[] values ;
	/** number of times of the summation. */
	private int counter;
	
	/**
	 * constructor for the Double summation test.
	 * @param counter number of times of the summation.
	 * @param arraySize Double array's size that will be used in the test.
	 */
	public SumDouble (int counter, int arraySize){
		this.counter = counter;
		this.values = new Double[arraySize];
		for(int i = 0; i < arraySize; i++){
			values[i] = new Double(i+1);
		}
	}
	
	/**
	 * run the test.
	 */
	public void run(){
		Double sum = new Double(0);
		for(int i = 0, count = 0; count < values.length ; i++,count++){
			if(i>=values.length) i = 0;
			sum = sum + (values[i]);
		}
	}
	
	/**
	 * give the information of the SumDouble.
	 * @return information of the test
	 */
	public String toString(){
		return String.format("%s with count=%d and array size=%d", NAME, counter, values.length);
	}
}
