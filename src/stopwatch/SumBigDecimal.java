package stopwatch;

import java.math.BigDecimal;

/**
 * BigDecimal summation test for speedtest.
 * @author Chinthiti Wisetsombat
 * @version 1.1
 */
public class SumBigDecimal implements Runnable{
	/** name of the test. */
	private static final String NAME = "Sum BigDecimal";
	/** array of BigDecimal that will be used to get the summation. */
	private BigDecimal[] values;
	/** number of times of the summation. */
	private int counter;
	
	/**
	 * constructor for BigDecimal summation test.
	 * @param counter number of times of the summation.
	 * @param arraySize size of BigDecimal array that will be used in the test
	 */
	public SumBigDecimal(int counter,int arraySize){
		this.counter = counter;
		this.values = new BigDecimal[arraySize];
		for(int i = 0; i < arraySize; i++){
			values[i] = new BigDecimal(i+1);
		}
	}
	
	/**
	 * run the test.
	 */
	public void run(){
		BigDecimal sum = new BigDecimal(0);
		for(int i = 0, count = 0 ; count < counter ; i++,count++){
			if(i >= values.length) i = 0;
			sum = sum.add(values[i]);
		}
	}
	
	/**
	 * give the information of the test.
	 * @return information of the test
	 */
	public String toString(){
		return String.format("%s with count=%d and array size=%d",NAME ,counter ,values.length);
	}
}
