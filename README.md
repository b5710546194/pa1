# Stopwatch by Chinthiti Wisetsombat (5710546194)

I ran the tasks on my laptop, and got these results :

Task                                                               | Time
-------------------------------------------------------------------|------------:
Append to String with count=100000                                 | 3.523394 s
Append to StringBuilder with count=100000                          | 0.002544 s
Sum double primitive with count=100000000 and array size=500000    | 0.003154 s
Sum Double with count=100000000 and array size=500000              | 0.009993 s
Sum BigDecimal with count=100000000 and array size=500000          | 0.911997 s


## Explanation of Results
I think the reason why append char to String and StringBuilder is very difference in time because when you append char to String, java need to convert the String to StringBuilder and append the char and convert the StringBuilder to String again, but to append char to StringBuilder, java just append the char to the StringBuilder directly so there is no need of conversion. 

For the summation test, I think the double(primitive) summation is shortest because it is primitive data. Its summation is done directly, and I think the Double(object) summation takes about 3 times longer because Double objects can't be summed to gather directly, java have to create a new Double(object) to keep the result of the summation. For the longest one, I think the reason why it take longer than the other test is similar to the Double(object) but this one, BigDecimal , is bigger than Double it may take longer to create a new instance to keep the result.